# Add any directories, files, or patterns you don't want to be tracked by version control
-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 04, 2016 at 01:54 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webbanhang`
--

-- --------------------------------------------------------

--
-- Table structure for table `binhluansp`
--

CREATE TABLE `binhluansp` (
  `MaBL` varchar(10) NOT NULL,
  `MaSP` varchar(10) DEFAULT NULL,
  `TieuDe` varchar(100) DEFAULT NULL,
  `NoiDung` varchar(200) DEFAULT NULL,
  `HoTenBLV` varchar(100) DEFAULT NULL,
  `TrangThai` int(11) DEFAULT NULL COMMENT '0: Chờ Duyệt, 1: Duyệt'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `danhmucsanpham`
--

CREATE TABLE `danhmucsanpham` (
  `MaDM` varchar(10) NOT NULL,
  `TenDM` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `danhmucsanpham`
--

INSERT INTO `danhmucsanpham` (`MaDM`, `TenDM`) VALUES
('A', 'Áo'),
('G', 'Giầy'),
('Q', 'Quần');

-- --------------------------------------------------------

--
-- Table structure for table `doituongsudung`
--

CREATE TABLE `doituongsudung` (
  `MaNV` varchar(10) NOT NULL,
  `MaLoaiNV` varchar(10) DEFAULT NULL,
  `TenNV` varchar(100) DEFAULT NULL,
  `DiaChi` varchar(200) DEFAULT NULL,
  `SDT` varchar(15) DEFAULT NULL,
  `SoCMND` varchar(9) DEFAULT NULL,
  `ChucVu` varchar(50) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Quyen` varchar(2) DEFAULT NULL,
  `NgaySinh` date DEFAULT NULL,
  `Username` varchar(100) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loaihinhkhuyenmai`
--

CREATE TABLE `loaihinhkhuyenmai` (
  `MaLHKM` varchar(10) NOT NULL,
  `MaSP` varchar(10) DEFAULT NULL,
  `TenLHKM` varchar(200) DEFAULT NULL,
  `NgayBD` date DEFAULT NULL,
  `NgayKT` date DEFAULT NULL,
  `GhiChu` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loaisp`
--

CREATE TABLE `loaisp` (
  `MaLoai` varchar(10) NOT NULL,
  `MaDM` varchar(10) DEFAULT NULL,
  `TenLoai` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loaisp`
--

INSERT INTO `loaisp` (`MaLoai`, `MaDM`, `TenLoai`) VALUES
('ATRNU', 'A', 'Áo tranning nữ'),
('ATTN', 'A', 'Áo thể thao nam'),
('GN', 'G', 'Giầy nam'),
('GNU', 'G', 'Giầy nữ'),
('QTTN', 'Q', 'Quần thể thao nam'),
('QTTNU', 'Q', 'Quần thể thao nữ');

-- --------------------------------------------------------

--
-- Table structure for table `muaban`
--

CREATE TABLE `muaban` (
  `MaNV` varchar(10) NOT NULL,
  `MaSP` varchar(10) DEFAULT NULL,
  `MaPDH` varchar(10) DEFAULT NULL,
  `SoLuong` int(11) DEFAULT NULL,
  `ChietKhau` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nhacungcap`
--

CREATE TABLE `nhacungcap` (
  `MaNCC` varchar(10) NOT NULL,
  `TenNCC` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nhacungcap`
--

INSERT INTO `nhacungcap` (`MaNCC`, `TenNCC`) VALUES
('AD', 'Adidas'),
('Ni', 'Nike');

-- --------------------------------------------------------

--
-- Table structure for table `phanloaidoituong`
--

CREATE TABLE `phanloaidoituong` (
  `MaLoaiNV` varchar(10) NOT NULL,
  `TenLoaiNV` varchar(100) DEFAULT NULL,
  `ChinhSach` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `phieudathang`
--

CREATE TABLE `phieudathang` (
  `MaPDH` varchar(10) NOT NULL,
  `HoTenNM` varchar(200) DEFAULT NULL,
  `DiaChi` varchar(200) DEFAULT NULL,
  `CMND` varchar(5) DEFAULT NULL,
  `NgayGiaoHang` date DEFAULT NULL,
  `NgayDatHang` date DEFAULT NULL,
  `TongTien` int(11) DEFAULT NULL,
  `HienTrang` tinyint(4) DEFAULT NULL COMMENT '0: Đang xử lý, 1: Hoàn thành'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sanpham`
--

CREATE TABLE `sanpham` (
  `MaSP` varchar(10) NOT NULL,
  `MaLoai` varchar(10) DEFAULT NULL,
  `MaNCC` varchar(10) DEFAULT NULL,
  `TenSP` varchar(100) DEFAULT NULL COMMENT '0: Hết, 1: Còn',
  `TrangThai` tinyint(1) DEFAULT NULL,
  `NgayDang` date DEFAULT NULL,
  `MoTa` varchar(100) DEFAULT NULL,
  `GiaBan` int(11) DEFAULT NULL,
  `HinhAnh` varchar(100) DEFAULT NULL,
  `ThoiGianBaoHanh` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sanpham`
--

INSERT INTO `sanpham` (`MaSP`, `MaLoai`, `MaNCC`, `TenSP`, `TrangThai`, `NgayDang`, `MoTa`, `GiaBan`, `HinhAnh`, `ThoiGianBaoHanh`) VALUES
('001', 'GN', 'AD', 'adidas giayda-banh x-16.1-firm-ground-cleats-s81939-vi', 1, '2016-11-20', NULL, 1000000, 'WBHTL/images/sanpham/adidas giayda-banh x-16.1-firm-ground-cleats-s81939-vi.png', '12 month'),
('002', 'GN', 'AD', 'adidas tennis barricade-2016-adicolor', 30, '2016-11-20', NULL, 500000, 'WBHTL/images/sanpham/adidas tennis barricade-2016-adicolor.png', '1 day'),
('003', 'GN', 'AD', 'adidas vs-easy-vulc-summer-shoes', 0, '2016-11-20', NULL, 2000000, 'WBHTL/images/sanpham/adidas vs-easy-vulc-summer-shoes.png', '10 day'),
('004', 'ATTN', 'AD', 'ao-running-adidas-supernova-climachill-nam-ao1564', 2, '2016-11-20', NULL, 300000, 'WBHTL/images/sanpham/ao-running-adidas-supernova-climachill-nam-ao1564.jpg', '4 day'),
('005', 'ATTN', 'NI', 'ao-running-nike-as-dri-fit-cool-miler-singlet-nam-718347-010', 3, '2016-11-20', NULL, 4000000, 'WBHTL/images/sanpham/ao-running-nike-as-dri-fit-cool-miler-singlet-nam-718347-010.jpg', '6 day'),
('006', 'ATTN', 'NI', 'ao-running-nike-as-nike-dri-fit-contour-ss-nam-683518-100', 6, '2016-11-20', NULL, 5000000, 'WBHTL/images/sanpham/ao-running-nike-as-nike-dri-fit-contour-ss-nam-683518-100.jpg', '7 day');

-- --------------------------------------------------------

--
-- Table structure for table `tintuc`
--

CREATE TABLE `tintuc` (
  `MaTT` varchar(10) NOT NULL,
  `MaNV` varchar(10) DEFAULT NULL,
  `TenTT` varchar(100) DEFAULT NULL,
  `NoiDung` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `binhluansp`
--
ALTER TABLE `binhluansp`
  ADD PRIMARY KEY (`MaBL`),
  ADD UNIQUE KEY `MaBL` (`MaBL`,`MaSP`,`TieuDe`,`NoiDung`,`HoTenBLV`,`TrangThai`),
  ADD KEY `MaSP` (`MaSP`);

--
-- Indexes for table `danhmucsanpham`
--
ALTER TABLE `danhmucsanpham`
  ADD PRIMARY KEY (`MaDM`),
  ADD UNIQUE KEY `MaDM` (`MaDM`,`TenDM`);

--
-- Indexes for table `doituongsudung`
--
ALTER TABLE `doituongsudung`
  ADD PRIMARY KEY (`MaNV`),
  ADD UNIQUE KEY `MaNV` (`MaNV`,`MaLoaiNV`,`TenNV`,`DiaChi`,`SDT`,`SoCMND`,`ChucVu`,`Email`,`Quyen`,`NgaySinh`,`Username`,`Password`),
  ADD KEY `MaLoaiNV` (`MaLoaiNV`);

--
-- Indexes for table `loaihinhkhuyenmai`
--
ALTER TABLE `loaihinhkhuyenmai`
  ADD PRIMARY KEY (`MaLHKM`),
  ADD UNIQUE KEY `MaLHKM` (`MaLHKM`,`MaSP`,`TenLHKM`,`NgayBD`,`NgayKT`,`GhiChu`),
  ADD KEY `MaSP` (`MaSP`);

--
-- Indexes for table `loaisp`
--
ALTER TABLE `loaisp`
  ADD PRIMARY KEY (`MaLoai`),
  ADD UNIQUE KEY `MaLoai` (`MaLoai`,`MaDM`,`TenLoai`),
  ADD KEY `MaDM` (`MaDM`);

--
-- Indexes for table `muaban`
--
ALTER TABLE `muaban`
  ADD PRIMARY KEY (`MaNV`),
  ADD UNIQUE KEY `MaNV` (`MaNV`,`MaSP`,`MaPDH`,`SoLuong`,`ChietKhau`),
  ADD KEY `MaPDH` (`MaPDH`),
  ADD KEY `MaSP` (`MaSP`);

--
-- Indexes for table `nhacungcap`
--
ALTER TABLE `nhacungcap`
  ADD PRIMARY KEY (`MaNCC`),
  ADD UNIQUE KEY `MaNCC` (`MaNCC`);

--
-- Indexes for table `phanloaidoituong`
--
ALTER TABLE `phanloaidoituong`
  ADD PRIMARY KEY (`MaLoaiNV`),
  ADD UNIQUE KEY `MaLoaiNV` (`MaLoaiNV`,`TenLoaiNV`,`ChinhSach`);

--
-- Indexes for table `phieudathang`
--
ALTER TABLE `phieudathang`
  ADD PRIMARY KEY (`MaPDH`),
  ADD UNIQUE KEY `MaPDH` (`MaPDH`,`HoTenNM`,`DiaChi`,`CMND`,`NgayGiaoHang`,`NgayDatHang`,`TongTien`,`HienTrang`);

--
-- Indexes for table `sanpham`
--
ALTER TABLE `sanpham`
  ADD PRIMARY KEY (`MaSP`),
  ADD UNIQUE KEY `MaSP` (`MaSP`,`MaLoai`,`MaNCC`,`TenSP`,`TrangThai`,`NgayDang`,`MoTa`,`GiaBan`,`HinhAnh`,`ThoiGianBaoHanh`),
  ADD KEY `MaSP_2` (`MaSP`,`MaLoai`,`MaNCC`,`TenSP`,`TrangThai`,`NgayDang`,`MoTa`,`GiaBan`,`HinhAnh`,`ThoiGianBaoHanh`),
  ADD KEY `MaNCC` (`MaNCC`),
  ADD KEY `MaLoai` (`MaLoai`);

--
-- Indexes for table `tintuc`
--
ALTER TABLE `tintuc`
  ADD PRIMARY KEY (`MaTT`),
  ADD UNIQUE KEY `MaTT` (`MaTT`,`MaNV`,`TenTT`,`NoiDung`),
  ADD KEY `MaNV` (`MaNV`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `binhluansp`
--
ALTER TABLE `binhluansp`
  ADD CONSTRAINT `binhluansp_ibfk_1` FOREIGN KEY (`MaSP`) REFERENCES `sanpham` (`MaSP`);

--
-- Constraints for table `doituongsudung`
--
ALTER TABLE `doituongsudung`
  ADD CONSTRAINT `doituongsudung_ibfk_1` FOREIGN KEY (`MaLoaiNV`) REFERENCES `phanloaidoituong` (`MaLoaiNV`);

--
-- Constraints for table `loaihinhkhuyenmai`
--
ALTER TABLE `loaihinhkhuyenmai`
  ADD CONSTRAINT `loaihinhkhuyenmai_ibfk_1` FOREIGN KEY (`MaSP`) REFERENCES `sanpham` (`MaSP`);

--
-- Constraints for table `loaisp`
--
ALTER TABLE `loaisp`
  ADD CONSTRAINT `loaisp_ibfk_1` FOREIGN KEY (`MaDM`) REFERENCES `danhmucsanpham` (`MaDM`);

--
-- Constraints for table `muaban`
--
ALTER TABLE `muaban`
  ADD CONSTRAINT `muaban_ibfk_1` FOREIGN KEY (`MaNV`) REFERENCES `doituongsudung` (`MaNV`),
  ADD CONSTRAINT `muaban_ibfk_2` FOREIGN KEY (`MaPDH`) REFERENCES `phieudathang` (`MaPDH`),
  ADD CONSTRAINT `muaban_ibfk_3` FOREIGN KEY (`MaSP`) REFERENCES `sanpham` (`MaSP`);

--
-- Constraints for table `sanpham`
--
ALTER TABLE `sanpham`
  ADD CONSTRAINT `sanpham_ibfk_1` FOREIGN KEY (`MaNCC`) REFERENCES `nhacungcap` (`MaNCC`),
  ADD CONSTRAINT `sanpham_ibfk_2` FOREIGN KEY (`MaLoai`) REFERENCES `loaisp` (`MaLoai`);

--
-- Constraints for table `tintuc`
--
ALTER TABLE `tintuc`
  ADD CONSTRAINT `tintuc_ibfk_1` FOREIGN KEY (`MaNV`) REFERENCES `doituongsudung` (`MaNV`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
